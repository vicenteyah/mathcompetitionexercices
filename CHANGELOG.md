# Change log

## v1.0.0 -September. 12, 2019
    Initial release.

    **Added**
    .gitignore
    algoritms.py
    main.py
    README.md
## v1.0.0 -september. 13, 2019
    **Added**
    algorirthms added like fibonnacci
    euclidis algorithm
    perfect number.

## v1.0.0 -september. 15, 2019
    Second Changes

    **Added**
    CHANGELog.md
## v1.0.1 -september. 16, 2019
    **Changes**
    Third changes
    armstrong algorithm was modified and solved
## v1.0.1 -september. 29, 2019
    **Changes**
    armstrong algorithm was refactored
    fibonacci algorithm was refactored
    subprocess to clear screen added

    **Added**
    wlambert taylor series added and completed
    
## Contributors 
    Vicente Yah Dzul
    Fernando Herrera Garnica

