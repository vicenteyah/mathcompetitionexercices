![alternativetext](https://aaai.memberclicks.net/assets/site/aaai.png)
# AAAI Student Chapter Mérida México Applied Math Division
## Basic Math algorithms in python

> This program was developed to understand the math logic and programming logic at the same time.
> This project was created to Computacional Mathematics Competition for AAAI student chapter
> for Applied Math division at instituto Tecnológico de Mérida 
> Developed by vicente Yah and Fernando Herrera

## How does it work this program?
### To run the script,type the following command in your linux or mac terminal
### but you have to be at the same directory before running the command
```bash
$~ python3 main.py
```