from time import sleep
from controllers import *
import subprocess as sp
#euclid's Theorem
def  euclidesAlgorithm():
    p = int(input("please enter the first number: "))
    q = int(input("please enter the second number: "))
    c = 1
    while c != 0:
        r = p%q # it saves the module of division
        m = (p-r)/q
        sleep(0.5)
        print(" %d = (%d * %d) + %d"%(p,q,m,r))
        if r != 0 :
            p = q
            q = r
            c = 1
        else:
            c = 0
            print("the GCD is: %d"%(q))
            sleep(1)
    sp.call('clear', shell=True)


def fibonacciSequence():
    limit = int(input(" please enter the limit of the sequence"))
    if (limit <= 0):
        print("please enter a positive integer != 0")
    else:
        print("fibonacci sequence")
        for i in range(limit):
            print(f(i))
    sp.call('clear', shell=True)


def PerfectNumber():# logic problem
    num = int(input("please  enter an integer number: "))
    s=0

    for i in range(1,num):
        if(num % i == 0):
            s = s + i
            print("is divisor: %d "%i)
            sleep(0.5)
    if(s == num):
        print("%d is a perfect number"%num)
        sleep(5)
    else:
        print("%d this is not a perfect number"%num)
        sleep(5)
    sp.call('clear',shell =True)


def armstrongNumber():
    test = int(input(" please enter a number to know if is an armstrong number "))
    flag = True 
    counter = 0
    for i in range(0,2):
        if (i == 0):
            counter = rebuildNumber(flag,test,counter)
        else:
            flag = False
            rebuiltNumber = rebuildNumber(flag,test,counter)

    ComparisonToArmstrong(test,rebuildNumber)
    sp.call('clear', shell=True)


def WlambertFucntion():
    x = float(input("please enter x value  to compute the serie: "))
    n = int(input("please enter the limit odf the serie: "))
    taylorSeriesWlambert(x,n)
    sp.call('clear', shell=True)



def menu():
    menu = """
    [ 1 ] -> compute the GCD(greatest common divisor) given two numbers *
    [ 2 ] -> compute the fibonacci sequence using recursive relation
    [ 3 ] -> compute the perfect number
    [ 4 ] -> compute if a given number is armstrong
    [ 5 ] -> compute the taylor series W lambert function w0(x)=∑ ([(-n)^(n-1)]/n!)(x^n), to |x| < 1/e
    [ 6 ] -> compute the taylor series arcsin x = ∑ ((2n!)/([4^n]*n!*(2n+1)))(x^(2n+1)), to |x| < 1
    [ 7 ] -> compute if a given matrix is an idempotent matrix
    [ 8 ] -> compute det(A) using cofactors method
    [ 10 ] -> type number 10 to leave....
    """
    print(menu)


def selectMenu():
    while True:
        menu()
        option = int(input("type an option: "))
        if option == 1:
            sp.call('clear',shell =True)
            euclidesAlgorithm()
            sp.call('clear',shell =True)
        elif option == 2:
            sp.call('clear', shell=True)
            fibonacciSequence()
            sp.call('clear', shell=True)

        elif option == 3:
            sp.call('clear', shell=True)
            PerfectNumber()
            sp.call('clear', shell=True)

        elif option == 4:
            sp.call('clear', shell=True)
            armstrongNumber() 
            sp.call('clear', shell=True)
        elif option == 5:
            sp.call('clear', shell=True)
            WlambertFucntion()
            sp.call('clear', shell=True)
        elif option == 6:
            pass
        elif option == 10 :
            sleep(1)
            print("good bye ......:V")
            break

        

